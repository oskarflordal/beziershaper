# functions for lofting between two polygons
# these polygons are simplified in that they describe roughly the same shape
# and are in two separate planes above ano another
# this loft is made to work for shapes that are fairly similar and is likely to break down if mapping two completly misaligned shapes

import numpy as np
import math
from beziershaper import earcut
from shapely.geometry import Polygon
from shapely.geometry import Point
from shapely.geometry import LineString

def reformat(poly):
    return [[x,y] for x,y in zip(poly.exterior.xy[0],poly.exterior.xy[1])]

def distance_nonsqrt(v0,v1):
    return pow(v0[0]-v1[0], 2) + pow(v0[1]-v1[1], 2)

def reformat3d(poly, z):
    return [[x,y,z] for x,y in zip(poly.exterior.xy[0],poly.exterior.xy[1])]

def respace(poly, distance, maxdistance):
    """Try to simplify points that are to close and add extra points when distance is too
    big to get polys that are more loft friendly. simplification is done with shaely"""
    if (poly.area == 0):
        return poly

    poly = poly.simplify(distance)

    coords = poly.exterior.coords[:]

    newcoords = [coords[0]]
    idx = 0
    while idx+1 < len(coords):
        cur  = coords[idx]
        next = coords[idx+1]

        # when distance is above threshold split the distance in more points according to the max distance
        if (distance_nonsqrt(cur, next) > (maxdistance)**2):
            extra_steps = int(math.sqrt(distance_nonsqrt(cur, next))/(maxdistance))
            diffx = (cur[0] - next[0])/extra_steps
            diffy = (cur[1] - next[1])/extra_steps
            for x in range(1, extra_steps):
                newcoords.append([cur[0] - diffx*x, cur[1] - diffy*x])
        newcoords.append(next)
        idx = idx + 1

    return Polygon(newcoords)

def buildearcuttris(tris, data, height, dir):
  topface = earcut.earcut(data['vertices'], data['holes'], data['dimensions'])

  # write all the triangels (v0x,v0y,v0z,v1x,v1y, v1z,v2x,v2y,v2z,n0,n1,n2)
  if (dir == -1):
    tris.append([[data['vertices'][2 * topface[t + 2]], data['vertices'][2 * topface[t+2] + 1], height,
                  data['vertices'][2 * topface[(t + 1)]], data['vertices'][2 * topface[(t + 1)] + 1], height,
                  data['vertices'][2 * topface[(t + 0)]], data['vertices'][2 * topface[(t + 0)] + 1], height,
                  0, 0, -1] for t in range(0, len(topface), 3)])
  else:
    tris.append([[data['vertices'][2 * topface[t]], data['vertices'][2 * topface[t] + 1], height,
                  data['vertices'][2 * topface[(t + 1)]], data['vertices'][2 * topface[(t + 1)] + 1], height,
                  data['vertices'][2 * topface[(t + 2)]], data['vertices'][2 * topface[(t + 2)] + 1], height,
                  0, 0, 1] for t in range(0, len(topface), 3)])

def build_flat(tris, mainpoly, zoffset, outerpolys, dir):
  # polys can be cut into multiple parts we pretend they are always lists like this
  hullpart = mainpoly
  earcutpoly = [reformat(hullpart)]
  for op in outerpolys:
    earcutpoly.append(loft.reformat(op))
  data = earcut.flatten(earcutpoly)
  buildearcuttris(tris, data, zoffset, -dir)

def bypassisolated(poly, otherpoly, maxdistance):
    """Find any newly isolated areas that were removed in the buffered otherpoly. Retruns an updated poly and a list of isolated areas"""
    coords = poly.exterior.coords[:]

    #make sure we start at a nearby point
    for x in range(len(coords)):
        if otherpoly.exterior.distance(Point(coords[0])) < maxdistance:
            break
        else:
            coords = coords[1:] + coords[:1]

    newcoords = []
    newlists = []
    inisolated = False
    curisolated = []
    last = coords[-1]
    for x in coords:
        if otherpoly.exterior.distance(Point(x)) < maxdistance:
            if (inisolated):
                curisolated.append(x)
                newlists.append(curisolated)
                curisolated = []
                inisolated = False

            newcoords.append(x)
        else:
            # are we adding to a new isolate area or not
            if not inisolated:
                curisolated = [last]
                inisolated = True

            curisolated.append(x)
        last = x
    if (len(curisolated) != 0):
        curisolated.append(coords[0])
        newlists.append(curisolated)

    return Polygon(newcoords), [Polygon(x) for x in newlists]

def cross(v0, v1, v2):
    """give z direction of the triangle v0 v1 v2. Using standard cross product"""
    ax = v1[0]-v0[0]
    ay = v1[1]-v0[1]
    bx = v2[0]-v0[0]
    by = v2[1]-v0[1]

    return ax*by - bx*ay

def shortestinlist(l0, l1):
  ans = []
  for x in l1:
    dists = [distance_nonsqrt(x,y) for y in l0]
    ans.append(dists.index(min(dists)))

  for idx,x in enumerate(ans):
    if x != 0:
      return idx
  return len(ans)


def loft (poly0_shapely, poly1_shapely, poly0z, poly1z, flip, bypass_isolated = False):
    """This returns a list of triangels on the form (v0x,v0y,v0z,v1x,v1y, v1z,v2x,v2y,v2z,n0,n1,n2).
    It implements a simple lofting described at the top of the file"""
    poly0 = reformat(poly0_shapely)
    poly1 = reformat(poly1_shapely)[:-1]

    tris = []
    # find closest point to point 0 in poly0
    # make sure this point doesn't point and some other close but unrealted edge
    # could also be solved by taking the very shortest of all points and starting with that
    dists = [distance_nonsqrt(poly0[0], v1) + (
        1000 if LineString([poly0[0], v1]).crosses(poly0_shapely) else 0) for v1 in poly1]
    pos = dists.index(min(dists))

    # the polys will be circular and the last vertex is a copy of the first. make sure
    # when we are rotating we leave that copy out and add a new vertex as the last
    poly1 = poly1[pos:] + poly1[:pos] + poly1[pos:pos+1]

    l0 = poly0[:]
    l1 = poly1[:]

    while (len(l0) != 1 or len(l1) != 1):
        # Hack to get rid of areas of large isolated points when doing iterative buffering
        # would need a more clever lofting algorithm to sole this properly
        if bypass_isolated and len(l0) > 1 and poly1_shapely.exterior.distance(Point(l0[1])) > 20:
            b0 = l1
            b1 = l0
            b0z = poly1z
            b1z = poly0z
            dir = 1
        elif bypass_isolated and len(l1) > 1 and poly0_shapely.exterior.distance(Point(l1[1])) > 20:
            b0 = l0
            b1 = l1
            b0z = poly0z
            b1z = poly1z
            dir = 0
        else:
            if (len(l1) > 10 and len(l0) > 10):
                # find out if the other edges have a good reason to point to the first one on the other curve
                shortestl1 = shortestinlist(l0[0:10], l1[1:11])
                shortestl0 = shortestinlist(l1[0:10], l0[1:11])

                if shortestl1 > shortestl0:
                    b0 = l0
                    b1 = l1
                    b0z = poly0z
                    b1z = poly1z
                    dir = 0
                elif (shortestl0 > shortestl1):
                    b0 = l1
                    b1 = l0
                    b0z = poly1z
                    b1z = poly0z
                    dir = 1
                else:
                    if (distance_nonsqrt(l0[0], l1[1]) < distance_nonsqrt(l1[0], l0[1])):
                        b0 = l0
                        b1 = l1
                        b0z = poly0z
                        b1z = poly1z
                        dir = 0
                    else:
                        b0 = l1
                        b1 = l0
                        b0z = poly1z
                        b1z = poly0z
                        dir = 1

            elif not (len(l1) == 1) and \
                    ((len(l0) == 1) or (distance_nonsqrt(l0[0], l1[1]) < distance_nonsqrt(l1[0], l0[1]))):
                b0 = l0
                b1 = l1
                b0z = poly0z
                b1z = poly1z
                dir = 0
            else:
                b0 = l1
                b1 = l0
                b0z = poly1z
                b1z = poly0z
                dir = 1

        #Add one triangle from this list
        tris.append(build_triangle(dir, b0[0],b1[0],b1[1], b0z, b1z, b1z, flip))
        b1.pop(0)

    return tris

def findshortest(poly0, poly1):
    res = []
    # find closest matches for all
    shortest_of_all = 99999
    shortest_of_all_idx = 0

    for idx, p0 in enumerate(poly0):
        dists = [distance_nonsqrt(p0, v1) for v1 in poly1]
        m = min(dists)
        pos = dists.index(m)
        res.append(pos)

        if shortest_of_all > m:
            shortest_of_all_idx = idx
            shortest_of_all = m

    return res, shortest_of_all_idx

def ultraloft (poly0_shapely, poly1_shapely, poly0z, poly1z, flip, debug = False, check = False):
    """This returns a list of triangels on the form (v0x,v0y,v0z,v1x,v1y, v1z,v2x,v2y,v2z,n0,n1,n2).
    It implements a simple lofting described at the top of the file"""
    # we remove the duplicate start/end vertex since we will be manipulating the starting position and 
    # need to avoid overlapping/duplicate polys
    poly0 = reformat(poly0_shapely)[:-1]
    poly1 = reformat(poly1_shapely)[:-1]

    tris = []

    dists0, minofall0 = findshortest(poly0, poly1)
    dists1, minofall1 = findshortest(poly1, poly0)

    if (debug):
        plt.plot(*poly0_shapely.exterior.xy, linestyle='--', marker='o')
        plt.plot(*poly1_shapely.exterior.xy, linestyle='dashed', marker='o')

    prange0 = list(range(len(poly0)))
    prange1 = list(range(len(poly1)))

    # find closest point to point 0 in poly0
    # make sure this point doesn't point and some other close but unrelated edge
    # could also be solved by taking the very shortest of all points and starting with that
    l0 = prange0[minofall0:] + prange0[:minofall0]
    l1 = prange1[dists0[minofall0]:] + prange1[:dists0[minofall0]]

    # re add the first entries of the list to make the poly connect back
    l0.append(l0[0])
    l1.append(l1[0])

    if check:
        lines = []

    while (len(l0) != 1 or len(l1) != 1):
        if (len(l0) > 1 and len(l1) > 1):
            if dists0[l0[0]] == l1[1] or dists1[l1[1]] == l0[0]:
                b0 = poly0[l0[0]]
                b1 = poly1[l1[0]]
                b11 = poly1[l1[1]]
                b0z = poly0z
                b1z = poly1z
                dir = 0
                l1.pop(0)

            elif dists1[l1[0]] == l0[1] or dists0[l0[1]] == l1[0]:
                b0 = poly1[l1[0]]
                b1 = poly0[l0[0]]
                b11 = poly0[l0[1]]
                b0z = poly1z
                b1z = poly0z
                dir = 1
                l0.pop(0)
            else:
                if (distance_nonsqrt(poly0[l0[0]], poly1[l1[1]]) <
                        distance_nonsqrt(poly1[l1[0]], poly0[l0[1]])):
                    b0 = poly0[l0[0]]
                    b1 = poly1[l1[0]]
                    b11 = poly1[l1[1]]
                    l1.pop(0)
                    b0z = poly0z
                    b1z = poly1z
                    dir = 0
                else:
                    b0 = poly1[l1[0]]
                    b1 = poly0[l0[0]]
                    b11 = poly0[l0[1]]
                    b0z = poly1z
                    b1z = poly0z
                    dir = 1
                    l0.pop(0)
        else:

            if not (len(l1) == 1) and \
                    ((len(l0) == 1) or (distance_nonsqrt(poly0[l0[0]], poly1[l1[1]]) <
                                        distance_nonsqrt(poly1[l1[0]], poly0[l0[1]]))):
                b0 = poly0[l0[0]]
                b1 = poly1[l1[0]]
                b11 = poly1[l1[1]]
                b0z = poly0z
                b1z = poly1z
                dir = 0
                l1.pop(0)
            else:
                b0 = poly1[l1[0]]
                b1 = poly0[l0[0]]
                b11 = poly0[l0[1]]
                b0z = poly1z
                b1z = poly0z
                dir = 1
                l0.pop(0)

        #Add one triangle from this list
        tris.append(build_triangle(dir, b0,b1,b11, b0z, b1z, b1z, flip))
        if debug:
            plt.plot([b0[0], b11[0]], [b0[1], b11[1]])

    if debug:
        plt.show()

    return tris

def addnormal(verts):
    v0 = np.asarray(verts[0:3])
    v1 = np.asarray(verts[3:6])
    v2 = np.asarray(verts[6:9])

    A = v1-v0
    B = v2-v0
    N = -np.cross(A,B)
    return verts + N.tolist()

def build_triangle(dir, v0, v1, v2, v0z, v1z, v2z, flip):
    """This returns a triangel on the form (v0x,v0y,v0z,v1x,v1y, v1z,v2x,v2y,v2z,n0,n1,n2)"""
    v0 = np.asarray(v0 + [v0z])
    # reverse order to get the normal correct
    # todo cleaner to have outside
    if (dir):
        v1, v2, v1z, v2z = v2, v1, v2z, v1z
    v1 = np.asarray(v1 + [v1z])
    v2 = np.asarray(v2 + [v2z])
    # flip order to flip direction for normal
    if (flip):
        v0, v2 = v2, v0

    A = v1-v0
    B = v2-v0
    N = np.cross(A,B)

    return v0.tolist() + v1.tolist() + v2.tolist() + N.tolist()


