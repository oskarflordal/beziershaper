# Functionality for making a glyph into a poly
from ttfquery import describe
from ttfquery import glyphquery
import ttfquery.glyph as glyph
from shapely.geometry import Polygon

def bezierstep(coorArr, i, j, t):
  """# recursive bezier splint splitter. Apparently all ttf splines are with one control point,
  so could be skipped for simpler method"""
  if j == 0:
    return coorArr[i]
  return bezierstep(coorArr, i, j - 1, t) * (1 - t) + bezierstep(coorArr, i + 1, j - 1, t) * t

def decasteljau(points, steps = 64):
  """add all but the first point
  to the list by creating x steps"""
  outlist = []

  if (len(points) > 0):
    stepsize = 1./steps
    for x in range(1,steps+1):
      xpos = bezierstep([p[0][0] for p in points], 0, len(points)-1, x*stepsize)
      ypos = bezierstep([p[0][1] for p in points], 0, len(points)-1, x*stepsize)

      if len(outlist) == 0 or outlist[-1][0] != xpos or outlist[-1][1] != ypos:
        outlist.append([xpos,ypos])

  return outlist

def decasteljau_pre(curbezier):
  """to ttf splitting of back to back control point into quadractic beziers"""
  if (len(curbezier) <= 3):
    return decasteljau(curbezier)
  else:
    newcontrol = [(((curbezier[1][0][0]+curbezier[2][0][0])/2, (curbezier[1][0][1]+curbezier[2][0][1])/2), 1)]
    return decasteljau(curbezier[0:2] + newcontrol) + decasteljau_pre(newcontrol + curbezier[2:])

def unbezier(curve):
  """Every item in the list is ((x,y), flag) flag says 0 if control point, look for sequences to replace"""
  curbezier = [curve.pop(0)]
  outlist = [curbezier[0][0]]

  for c in curve:
    curbezier.append(c)

    if (curbezier[0][1] == curbezier[-1][1]):
      outlist = outlist + decasteljau_pre(curbezier)
      curbezier = [curbezier.pop()]
  return outlist


def to_polys(curves):
  return [Polygon(unbezier(a)) for a in curves]


def getg(char, font_url="arial_ttf"):
  "Generate beziers from font file, create polys from bezier"
  font = describe.openFont(font_url)
  g = glyph.Glyph(glyphquery.glyphName(font, char))
  contours = g.calculateContours(font)

  return to_polys(contours)
