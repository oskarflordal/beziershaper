import struct

def generate_binary_stl(tris, filename, scale=0.008, flip = True):
  print(filename)
  fo = open(filename, "wb")

  #header
  fo.write(bytearray(80))
  fo.write(struct.pack("<I", sum([len(t) for t in tris])))

  #write all the triangels (v0x,v0y,v0z,v1x,v1y, v1z,v2x,v2y,v2z,n0,n1,n2)
  for trisu in tris:
    for t in trisu:
      fo.write(struct.pack("<fff", t[9], t[10], t[11]))
      fo.write(struct.pack("<fff", (-1 if flip else 1)*t[0]*scale, t[1]*scale, t[2]*scale))
      fo.write(struct.pack("<fff", (-1 if flip else 1)*t[3]*scale, t[4]*scale, t[5]*scale))
      fo.write(struct.pack("<fff", (-1 if flip else 1)*t[6]*scale, t[7]*scale, t[8]*scale))
      fo.write(struct.pack("<h", 0))

  fo.close()