import numpy as np
import trimesh
import argparse

trimesh.util.attach_to_log()

parser = argparse.ArgumentParser(description='CHeck an stl')
parser.add_argument('--mdl', required=True, help='which stl to check')

args = parser.parse_args()

mesh = trimesh.load(args.mdl)

print(mesh)
print(mesh.is_watertight)